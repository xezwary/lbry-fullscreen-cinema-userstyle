# LBRY Fullscreen cinema Userstyle

Just a simple userstyle.

Move the mouse near the top of the screen to get the top nav bar.

The smaller movable player takes up 100% width on content pages, I'm not gonna
fix it.

If you can do better, or have anything better, go ahead.

License: Public Domain
I give everyone permission, rights, license, etc to do whatever you want with
this userstyle.